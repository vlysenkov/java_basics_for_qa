public class Chapter7Task3 {
    public static void main(String s[]) {
        Book book1 = new Book("Java, The Complete Reference");
        Book book2 = new Book("Java, The Complete Reference", "Herbert Schildt");
        Book book3 = new Book("Java, The Complete Reference", "Herbert Schildt", 8);

        System.out.println("Book 1 = " + book1.name);
        System.out.println("Book 2 = " + book2.name + " - " + book2.author);
        System.out.println("Book 3 = " + book3.name + " - " + book3.author + " - " + book3.edition);

    }
}

class Book {
    String name;
    String author;
    int edition;

    Book(String name){
        this.name = name;
    }

    Book(String name, String author){
        this(name);
        this.author = author;
    }

    Book(String name, String author, int edition){
        this(name, author);
        this.edition = edition;
    }
}
