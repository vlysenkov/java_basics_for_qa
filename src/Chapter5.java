public class Chapter5 {
    public static void main(String[] args) {
        int[] array = new int[10];
        array[0] = 100;
        array[1] = -50;
        array[2] = 0;
        array[3] = 99;
        array[4] = -1010;
        array[5] = 1;
        array[6] = 34;
        array[7] = 15;
        array[8] = 66;
        array[9] = 105;
        int maxVal = array[0];
        for (int i = 0; i < array.length; i++) {
            if (array[i] > maxVal) {
                maxVal = array[i];
            }
        }
        System.out.println(maxVal);
    }
}
